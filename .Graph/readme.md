Data Recovery for UniFS supported Filesystems!

# Guide
Best!
- https://wiki.archlinux.org/index.php/File_recovery

# Ext4 undelete
Discussion:
- https://unix.stackexchange.com/questions/122305/undelete-a-just-deleted-file-on-ext4-with-extundelete

Quote: "extundelete is designed to undelete files from an unmounted partition to a separate (mounted) partition. extundelete will restore any files it finds to a subdirectory of the current directory named “RECOVERED_FILES”. To run the program, type “extundelete --help” to see various options available to you.

Typical usage to restore all deleted files from a partition looks like this:

    $ extundelete /dev/sda4 --restore-all"

Sch:
- https://www.google.com/search?q=ext4+undelete

Code:
- http://extundelete.